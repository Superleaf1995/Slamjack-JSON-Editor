## Setup
Download both editors (WEDITOR and LEDITOR). Then place them in any folder you want. If youre not using Windows you will need to install DOSBox or any DOS emulator.

## Level Editor
The level editor requieres at first that you specify a filename with the level you want. Then you can start using the level editor. To scroll trough the screen you can use Page Down/Up to move trough the map. To move the cursor use the arrow keys of the keyboard.
As in the time of writing, the level editor only serves to view levels and calculate the optimal initial position.
If the editor runs low on DOSBox press CTRL+F12 until it is optimal for use.

### Key Mapping
* F1 - Save
* F2 - Set initial position to the current position of the cursor
* PG DWN - Scroll to the left
* PG UP - Scroll to the right
* a - Switch betwen parameters (left)
* d - Switch betwen parameters (right)
* ENTER - Input new parameter value
* ESC - Exit without saving
* Arrow Keys - Move betwen tiles (no auto scroll)

### Notes
All data in the world map will be overwritten, only 1 level per world map is saved as of now.

## World Editor
The world editor is quite simple, it can create blank worlds and modify general world parameters.
